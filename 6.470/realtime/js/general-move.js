// This section begins the data held in the "objMLB" object
// Normally, this would be in a database and would be available via Ajax or some other method
var currentItem = 0,
objMLB = {
	extURL: [
		"#0",
		"#1",
		"#2",
		"#3",
		"#4",
		"#5"
	] // extURL

}; // objMLB ends here

/*globals $ window document clearInterval setInterval setTimeout objMLB currentItem */
$(function () {
	var s = null,
	myTimer = null,
	// primary namespace begins here
	MyContent = {
		// below are the variables being used in the slider
		settings : {
			// below is the width of the slider, used when calculating the slide distance
			sliderWidth: $("#photo").width(),
			// below is the speed of the animated slider in miliseconds
			speed: 200, // higher = slower for moving speed
			// below is the time between each slide for the auto-sliding mechanism
			intervalPause: 2000, // works speed
			// the variables below are just caching stuff that's reused
			photoList: $("#photo ul"),
			links: $("#navigation ul a"),
			navLinks: $("#navigation ul a"),
			hlLinks: "#navigation a.highlight",
			hlClass: "highlight",
			prevNext: $(".prev-next"),
			playPause: $("#play-pause"),
			pnItem: 0,
			// below are just initial values for later-used stuff
			marginSetting: null,
			clickedURL: null,
			clickedHash: null,
			navURL: null,
			navHash: null,
			currentView: null,
			activeItem: null
		},

		// function to populate the data for the clicked item; data comes from file: "js/data.js"
		init: function (currentItem, objMLB) {
			s = this.settings;
			$("#title>a")[0].href = objMLB.extURL[currentItem];
		},


		// this triggers click functionality for the thumbs and circle links
		doClick: function () {
			s = this.settings;
			// bind a click event to the thumbnail photos and navigation links
			$(s.links).bind("click", function () {
				clearInterval(myTimer);
				s.playPause.removeClass().addClass("play");
				// get the clicked item from the URL
				s.clickedURL = this.href.toString();
				s.clickedHash = s.clickedURL.split("#")[1];
				// a previously existing highlight is removed to prevent duplicates
				$(s.links).removeClass(s.hlClass);

				// loop through the navigation links to highlight the clicked one
				s.navLinks.each(function () {
					s.navURL = this.href.toString();
					s.navHash = s.navURL.split("#")[1];
					if (s.clickedHash === s.navHash) {
						$(this).addClass(s.hlClass);
					}
				});

				// if any of items 2-6 are clicked, animate the margin accordingly
				if (s.clickedHash > 0) {
					s.marginSetting = s.sliderWidth * s.clickedHash;
					s.photoList.animate({
						marginLeft: "-" + s.marginSetting + "px"
					}, s.speed, function () {
						  // optional callback after animation completes
					});

				// if item 1 is clicked, send the switcher back to the front (0 margin)
				} else {
					s.photoList.animate({
							marginLeft: "0px"
						}, s.speed, function () {
							// optional callback after animation completes
					});
				}
				MyContent.init(s.clickedHash, objMLB);
				return false;
			});

		},

		prepPreviousNext: function () {

			s.prevNext.bind("click", function () {
				clearInterval(myTimer);
				s.playPause.removeClass().addClass("play");
				s.activeItem = $("#navigation a.highlight")[0].href.split("#")[1];

				// make sure the value from the URL is a number, otherwise addition operator won't work
				s.activeItem = parseInt(s.activeItem, 10);

				if ($(this).attr("id") === "prev") {
					s.pnItem = (s.activeItem + 5) % 6;
				} else {
					s.pnItem = (s.activeItem + 7) % 6;
				}

				$(s.links).removeClass(s.hlClass);
				MyContent.doPreviousNext(s.activeItem, s.pnItem);
				return false;
			});
		},
		
		// below are the previous/next actions
		doPreviousNext: function (activeItem, pnItem) {
			s = this.settings;
			$("#navigation ul li:nth-child(" + (s.pnItem + 1) + ") a").addClass(s.hlClass); //NTH-CHILD
		
			// calculate the animated margins
			if (s.pnItem > 0) {
				s.marginSetting = s.sliderWidth * s.pnItem;
				s.photoList.animate({
					marginLeft: "-" + s.marginSetting + "px"
				}, s.speed);
		
			} else {
				s.photoList.animate({
					marginLeft: "0px"
				}, s.speed);
			}
			this.init(s.pnItem, objMLB);
		},
		
		// Below are the auto-run actions
		// there's probably too much repeated here from other sections, but whatever
		doAutoRun: function () {
			s = this.settings;
			s.currentView = $(s.hlLinks)[0].href.split("#")[1];
			s.currentView = parseInt(s.currentView, 10);
			
			s.currentView = (s.currentView + 7) % 6

			s.navLinks.removeClass();
			
			$("#navigation ul li:nth-child(" + (s.currentView + 1) + ") a").addClass(s.hlClass);

			if (s.currentView > 0) {
				s.marginSetting = s.sliderWidth * s.currentView;
				s.photoList.animate({
					marginLeft: "-" + s.marginSetting + "px"
				}, s.speed);
		
			} else {
				s.photoList.animate({
					marginLeft: "0px"
				}, s.speed);
			}
			this.init(s.currentView, objMLB);
		},
		
		// play and pause the auto-run feature
		doPlayPause: function () {
			s = this.settings;
			s.playPause.click(function () {
				if ($(this).hasClass("pause")) {
					$(this).removeClass().addClass("play");
					clearInterval(myTimer);
				} else {
					$(this).removeClass().addClass("play pause");
					clearInterval(myTimer);
					myTimer = setInterval(function () {
						MyContent.doAutoRun();
					}, s.intervalPause);

				}
				return false;
			});
		}

	}; // primary MyContent namespace ends here
	
	// these are the function calls to trigger all the functionality
	// I have no idea if there is a better way to do this
	MyContent.init(currentItem, objMLB);
	MyContent.doClick();
	MyContent.prepPreviousNext();
	myTimer = setInterval(function () {
		MyContent.doAutoRun();
	}, MyContent.settings.intervalPause);
	MyContent.doPlayPause();
});