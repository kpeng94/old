var num = "<?php= ?>";
var currentItem = 0,
content = {
	extURL: [
		"#0",
		"#1",
		// "#2",
		// "#3",
		// "#4",
		"#5"
	] // extURL

}; // content ends here

/*globals $ window document clearInterval setInterval setTimeout content currentItem */
$(function () {
	var s = null,
	// primary namespace begins here
	MyContent = {
		// below are the variables being used in the slider
		settings : {
			sliderWidth: $("#photo").width() + 6,
			speed: 200, // higher = slower for moving speed
			// below is the time between each slide for the auto-sliding mechanism
			intervalPause: 2000, // works speed
			// the variables below are just caching stuff that's reused
			photoList: $("#photo ul"),
			canvas: $("#photo"),
			links: $("#navigation ul a"),
			navLinks: $("#navigation ul a"),
			hlLinks: "#navigation a.highlight",
			hlClass: "highlight",
			prevNext: $(".prev-next"),
			pnItem: 0,
			// below are just initial values for later-used stuff
			marginSetting: null,
			clickedURL: null,
			clickedHash: null,
			navURL: null,
			navHash: null,
			currentView: null,
			activeItem: null
		},

		// function to populate the data for the clicked item; data comes from file: "js/data.js"
		init: function (currentItem, content) {
			s = this.settings;
			$("#title>a")[0].href = content.extURL[currentItem];
		},
		prepPreviousNext: function () {

	          // $(window).keyup(function(event){
	          //   if(event.which == 37){
	              
	          //   }

	          // })
			s.prevNext.bind("click", function () {
				s.activeItem = $("#navigation a.highlight")[0].href.split("#")[1];

				// make sure the value from the URL is a number, otherwise addition operator won't work
				s.activeItem = parseInt(s.activeItem, 10);

				if ($(this).attr("id") === "prev") {
					s.pnItem = (s.activeItem + num - 1) % num;
				} else {
					s.pnItem = (s.activeItem + num + 1) % num;
				}

				$(s.links).removeClass(s.hlClass);
				MyContent.doPreviousNext(s.activeItem, s.pnItem);
				return false;
			});
		},
		
		// below are the previous/next actions
		doPreviousNext: function (activeItem, pnItem) {
			s = this.settings;
			$("#navigation ul li:nth-child(" + (s.pnItem + 1) + ") a").addClass(s.hlClass); //NTH-CHILD
			// $(".class ul li:nth-child(" + (s.pnItem + 1) + ") a".addClass(s.hlClass);

			// calculate the animated margins
			if (s.pnItem > 0) {
				s.marginSetting = s.sliderWidth * s.pnItem;
				s.photoList.animate({
					marginLeft: "-" + s.marginSetting + "px"
				}, s.speed);
		
			} else {
				s.photoList.animate({
					marginLeft: "0px"
				}, s.speed);
			}
			this.init(s.pnItem, content);
		},
		
		// Below are the auto-run actions
		// there's probably too much repeated here from other sections, but whatever

	}; // primary MyContent namespace ends here
	
	// these are the function calls to trigger all the functionality
	// I have no idea if there is a better way to do this
	MyContent.init(currentItem, content);
	MyContent.prepPreviousNext();
});