// This section begins the data held in the "objMLB" object
// Normally, this would be in a database and would be available via Ajax or some other method
var currentItem = 0,
objMLB = {
	headlineText: [
		"A",
		"B",
		"C",
		"D",
		"E",
		"F"
	], // headlineText
	
	smallCaption: [
		"A.",
		"B.",
		"C.",
		"D.",
		"E.",
		"F."
	], // smallCaption

	descText: [
		"A.",
		"B.",
		"C.",
		"D.",
		"E.",
		"F."
	], // descText

	extURL: [
		"#0",
		"#1",
		"#2",
		"#3",
		"#4",
		"#5"
	] // extURL

}; // objMLB ends here