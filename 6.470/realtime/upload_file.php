<?php
session_start();
require("db.php");
$_SESSION["uploads"] = "";

$displayStr = "";
// tmp_name = temporary copy
$allowedExts = array("jpg", "jpeg", "gif", "png"); // allowed extensions

//make a directory if it doesn't already exist
if(!is_dir("upload/" . $_POST["upload_select"] . "/" . $_POST["lecture"] . "/")){
  if(!is_dir("upload/" . $_POST["upload_select"] . "/"))
    mkdir("upload/" . $_POST["upload_select"] . "/");
  mkdir("upload/" . $_POST["upload_select"] . "/" . $_POST["lecture"] . "/");
}

if(!is_dir("classrooms/" . $_POST["upload_select"] . "/" . $_POST["lecture"] . "/")){
  if(!is_dir("classrooms/" . $_POST["upload_select"] . "/"))
    mkdir("classrooms/" . $_POST["upload_select"] . "/");
  mkdir("classrooms/" . $_POST["upload_select"] . "/" . $_POST["lecture"] . "/");
  copy("classrooms/6.00/1/redir.php", "classrooms/" . $_POST["upload_select"] . "/" . $_POST["lecture"] . "/" . "redir.php");
}

$upload = $_POST["upload_select"];
$lecture = $_POST["lecture"];

$count_query = "SELECT * FROM uploads WHERE CLASS = '$upload' and LECTURE = '$lecture'";
$count_result = mysql_query($count_query, $db) or die(mysql_error());

if((mysql_num_rows($count_result) + count($_FILES["file"]["tmp_name"])) <= 50){
  for($i = 0; $i < count($_FILES["file"]["tmp_name"]); $i++){
    $extension = end(explode(".", $_FILES["file"]["name"][$i]));

    if ((($_FILES["file"]["type"][$i] == "image/gif")
      || ($_FILES["file"]["type"][$i] == "image/jpeg")
      || ($_FILES["file"]["type"][$i] == "image/png")
      || ($_FILES["file"]["type"][$i] == "image/pjpeg"))
      && ($_FILES["file"]["size"][$i] < 2000000)
      && in_array($extension, $allowedExts)) /*checks for correct file type and extension is correct*/
    {
      if ($_FILES["file"]["error"][$i] > 0)
        $displayStr = $displayStr . $_FILES["file"]["name"][$i] . ": Return Code: " . $_FILES["file"]["error"][$i] . "<br />";
      else{
        if (file_exists("upload/" . $_POST["upload_select"] . "/" . $_POST["lecture"] . "/" . $_FILES["file"]["name"][$i]))
        {
          $displayStr = $displayStr . $_FILES["file"]["name"][$i] . " already exists in " . $_POST["upload_select"] . "/" . $_POST["lecture"] . "/" . " <br />";
        }
        else{
          move_uploaded_file($_FILES["file"]["tmp_name"][$i], "upload/" . $_POST["upload_select"] . "/" . $_POST["lecture"] . "/" . $_FILES["file"]["name"][$i]);
          $displayStr = $displayStr . "Stored in: " . $_POST["upload_select"] . "/" . $_POST["lecture"] . "/" . $_FILES["file"]["name"][$i] . "<br />";
          $filename = $_FILES["file"]["name"][$i];
          $query = "INSERT INTO uploads VALUES('$upload', '$lecture', '$filename')";
          mysql_query($query, $db) or die(mysql_error());
        }
      }
    }
    else
    {
      $displayStr = $displayStr . $_FILES["file"]["name"][$i] . " is an invalid file. <br />";
    }
  }
}
else{
  $displayStr = "You are trying to upload too many files! The maximum number of slides per lecture is 50. You current have " . mysql_num_rows($count_result) . " in this directory.";
}
  $_SESSION["uploads"] =  $displayStr;
  header("location:index2.php");
?>