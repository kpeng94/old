
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>realMITe</title>

    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="css/custom.css" rel="stylesheet">
  </head>

  <body>
  <?php
  $login_status = "";
  $register_status ="";
  session_start();
  if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
    header("location:index.php");
//    header("location:index2.php");
  }
  if(isset($_POST["user"]) && isset($_POST["pass"])){
    require_once("db.php");
    $user = $_POST["user"];
    $pass = sha1($_POST["pass"]);
    $query = "SELECT * from test1 WHERE USERNAME='" . mysql_real_escape_string($user) . "'";
    $result = mysql_query($query, $db) or die(mysql_error());
    $row = mysql_fetch_assoc($result);
    if($pass == $row["PASSWORD"]){
      $_SESSION['loggedin']= true;
      $_SESSION['first'] = $row["FIRST"];
      $_SESSION['last'] = $row["LAST"];
      $_SESSION['username'] = $user;
      $_SESSION['type'] = $row["TYPE"];
	    header("location:index.php");
//      header("location:index2.php");
      $login_status = "";
    }
    else{
      $login_status = "Invalid Entry!";
    }
  }

  if(isset($_POST["username"]) && isset($_POST["password"])){
    require("db.php");
    $user = mysql_real_escape_string($_POST["username"]);
    $query = "SELECT COUNT(*) FROM test1 WHERE USERNAME='$user'";
    $result = mysql_query($query, $db);
    $row = mysql_fetch_array($result);
    if ($row["COUNT(*)"] != 0) {
      $register_status = "The username $user has already been taken!";
    }
    else {
      $pass = sha1(mysql_real_escape_string($_POST["password"]));
      $confirm = sha1(mysql_real_escape_string($_POST["confirm"]));
      $first = mysql_real_escape_string($_POST["first"]);
      $type = "student";
      $last = mysql_real_escape_string($_POST["last"]);
      $query = "INSERT INTO test1 VALUES ('$user', '$pass', '$first', '$last', '$type')";
      if(strlen(mysql_real_escape_string($_POST["password"])) < 6){
        $register_status = "Password is too short.";
      }
      else if($pass == $confirm){
        mysql_query($query, $db) or die(mysql_error());
        $register_status = "Registration for $user was successful";  
        $success = true;
      }
      else{
        $register_status = "Passwords do not match";
      }
    }
  }
  ?>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          </a>
          <a class="brand" href="#">realMITe</a>
          <div class="nav-collapse collapse">
            <form action = "<?php $_SERVER["PHP_SELF"]?>"method="post" class="navbar-form pull-right">
              <input class="span2" type="text" placeholder="Username" name = "user">
              <input class="span2" type="password" placeholder="Password" name = "pass">
              <button type="submit" class="btn">Sign in</button>
            </form>               
          </div><!--/.nav-collapse -->
        </div>
     </div>
    <span class = "span2 invalid pull-right"><?php echo $login_status ?></span>                      
    </div>
    <div class="container">

      <div class="hero-unit">

          <h4 class = "">THE CONCEPT</h4>
          <p>realMITe, the #1 realTIMe education website for MIT students! Professors post their lecture slides on this site for students to view during class. Students can mark up the slides to ease note-taking.</p> 

            <form class = "align-right row navbar-form" action="<?php $_SERVER["PHP_SELF"]?>" method="post">
              <input class="span2" type="text" placeholder="First Name" name = "first"><input class="span2" type="text" placeholder="Last Name" name = "last"><br />
              <input class="span4" type="text" placeholder="Username" name = "username"><br />
              <input class="span4" type="password" placeholder="Password" name = "password"><br />
              <input class="span4" type="password" placeholder="Confirm Password" name="confirm"><br />
              <span class="invalidreg"><?php echo $register_status ?></span><br />
              <button type="submit" class="btn btn-primary btn-large">Register</button>
            </form>



      </div>

      <!-- Example row of columns -->
      <div class="row">
        <div class="textspan4">
          <h2>Post</h2>
          <p>Professors, post your lecture slides for students to have easier access.</p> 
        </div>
        <div class="textspan4">
          <h2>Draw</h2>
          <p>Students, take notes by putting your mouse over the slides and drawing on them!</p>
       </div>
        <div class="textspan4">
          <h2>Collaborate</h2>
          <p>Share your notes with a partner! Two makes it twice as easy!</p>
        </div>
      </div>

      <hr>

      <footer>
        <p>&copy; realMITe 2013</p>
      </footer>

    </div> <!-- /container -->
    <script src="css/bootstrap-dropdown.js"></script>
  </body>
</html>
