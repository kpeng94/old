<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>realMITe</title>

  <style type="text/css">
  body {
    padding-top: 60px;
    padding-bottom: 40px;
  }
  </style>
  <link href="css/custom.css" rel="stylesheet">
  <link rel="stylesheet" href="screen.css" type="text/css" media="screen" />
</head>

<script src="http://code.jquery.com/jquery-1.9.0.min.js"></script>

<body>
  <?php
  /* SESSIONS*/
  session_start();
  require("db.php");
  $class_query = "SELECT CLASS from classes WHERE USERNAME='" . mysql_real_escape_string($user) . "' ORDER BY CLASS";
  $class_result = mysql_query($class_query, $db) or die(mysql_error());
  $classes = "";

/*CHECK IF LOGGED IN*/
  if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
    $first = $_SESSION['first'];
    $last = $_SESSION['last'];
  }
  else{
    header("location:index.php");
  }

// COUNT NUMBER OF SLIDES
  $images = glob("upload/" . $_SESSION["class"] . "/" . $_SESSION["lectureNum"]. "/" . "*.jpg");
  $numOfImages = 0;
  foreach($images as $image){
    $numOfImages++;
  }

//Do Hashes for Ext in JS
  $hashes = '';
  for($i = 0; $i < $numOfImages; $i++)
    $hashes = $hashes . '"#' . $i . '",';

//INITIALIZE VARIABLES
  $width = 650;
  $height = 500;
  $border = 1;


  if(isset($_SESSION["class"]))
    $classNumber = $_SESSION["class"];
  if(isset($_SESSION["lectureNum"]))
    $lectureNumber = $_SESSION["lectureNum"];
  ?>

<!-- NAVIGATION BAR -->
  <div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        </a>
        <a class="brand" href="#">realMITe</a>
        <div class="nav-collapse collapse">
          <ul class="nav pull-right">
            <li class="dropdown">
              <a href="#" class ="dropdown-toggle" data-toggle="dropdown">Welcome, <?php echo("$first" . " " . "$last");  ?>!<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="index2.php">My Profile</a></li>
                <li class="divider"></li>
                <li><a href="#" id = "logout">Logout</a></li>
              </ul>                
            </li>
          </ul>

        </div>
      </div>
    </div>
  </div>

<!-- OUTPUT TEXT AND NAVI NUMBERS-->
  <?php
  $output_text = "You are not allowed here!";
  if(isset($_SESSION["lectureNum"]) && isset($_SESSION["lectureNum"])){
    $output_text = "";
    $navi = "";
    $i = 1;
    $images = glob("upload/" . $_SESSION["class"] . "/" . $_SESSION["lectureNum"] . "/" . "*.jpg");
    foreach($images as $image){
      $output_text = $output_text . '<li><img src ="' . $image . '" width="' . $width . 'px" height="' . $height . 'px" /></li>'; 
      $navi = $navi . '<li><a href="#' . $i . '"></a></li>';
      $i++;
    }
  ?>

<!-- THE CANVAS -->
  <div class="container">
    <div style="width: <?php echo $width ?>px; margin: 0 auto 0 auto;">
      <div id="wrapper" style = "<?php echo $width ?>">
        <div id="photo">
          <ul>
            <canvas id="canvas2" style = "z-index: 1; position: relative; margin-top: 40px" width="<?php echo($width * $numOfImages) . 'px'?>" height ="<?php echo $height ?>"></canvas>
            <canvas id="canvas" style = "z-index: 2; position: relative; margin-top: <?php echo (-1 * $height) ?>px" width="<?php echo($width * $numOfImages) . 'px'?>" height ="<?php echo $height ?>"></canvas>                
          </ul>
        </div>
        <div id="photo" class="base"; style= "margin-top: <?php echo (-1 * $height) ?>px">
          <ul id="photos" style = "z-index: 0; width: <?php echo(($width + 2 * $border)* $numOfImages) . 'px'?>; margin-left: 0">
            <?php echo $output_text ?>
          </ul>
        </div>
      </div>
        <button class = "save"> Save </button>
        <!-- <button class = "restore"> Restore </button> -->
        <button class = "canvasToggle"> Toggle canvas </button>
        Color: #<input type="text" class = "colorselect" value = "659b41"></input>
        <button class = "clear"> Clear</button><br/>
        The save button will save your CURRENT drawing. It overwrites any previous saves, including toggles.<br/>
        The toggle canvas button will show your last save or remove it from display. <br />
        The clear button will clear your current drawing.<br />
        Note: More slides means more time to load your last save. Give it a few seconds if it seems to not be working.<br />
      <div id="description" class="base">
        <h1 id="title"><a href="#"></a></h1>
      </div>
    </div>
  </div>
  <div id="navigation" class="base">
    <a href="#" id="prev" class="prev-next"><span>&lt;</span></a>
    <ul>
      <li><a href="#0" class="highlight"></a></li>
      <?php echo $navi ?>
    </ul>
    <a href="#" id="next" class="prev-next"><span>&gt;</span></a>
  </div>

<!-- LOGGING OUT -->
  <?php }?>
  <script>
  $(document).ready(function(){
    $("#logout").click(function(){
      window.location = "logout.php";
    });
  });    
  </script>

  <script src="css/bootstrap-dropdown.js"></script>
<!-- CANVAS SCRIPT -->
  <script>
  var imagefile = new Image();    
  var imagefileDrawn = false;
  window.setInterval(function(){
imagefile.src = "<?php echo 'classrooms/' . $_SESSION['class'] . '/' . $_SESSION['lectureNum'] . '/' . $_SESSION['username'] . '/markup.png'; ?>";

  // imagefile.src = "<?php echo 'classrooms/' . $_SESSION['class'] . '/' . $_SESSION['lectureNum'] . '/' . 'bobbob' . '/markup.png'; ?>";
}, 1000);
  layer = document.getElementById("canvas2");
  ctx = layer.getContext("2d");  
  $('.canvasToggle').click(function(){
    if(!imagefileDrawn){
      ctx.clearRect(0,0, <?php echo($width * $numOfImages)?>,<?php echo $height ?>);
      ctx.drawImage(imagefile, 0, 20);
    }
    else{
      canvas2.width = canvas2.width;

    }
      imagefileDrawn = !imagefileDrawn;
  });


  var clickX = new Array(); /*location of X and Y*/
  var clickY = new Array();
  var clickDrag = new Array(); 
  var clickColor = new Array();
  var clickTool = new Array();
  var pressed;

  // COLOR PALETTE
  var colorPurple = "#cb3594";
  var colorGreen = "#659b41";
  var colorYellow = "#ffcf33";
  var colorBrown = "#986928";

  var curColor = colorGreen;

  var curTool = "pen";

  function addClick(x, y, drag)
  {

    clickX.push(x);
    clickY.push(y);
    clickDrag.push(drag);
    if(curTool == "eraser"){
      clickColor.push("white");
    }else{
      clickColor.push(curColor);
    }
  }

  // Context of canvas
  context = document.getElementById('canvas').getContext("2d");

  /*canvas mousedown*/
  $('.colorselect').keyup(function(event){
    curColor = "#" + $('.colorselect').val();
  });

  $('#canvas').mousedown(function(e){
    var mouseX = e.pageX - this.offsetLeft;
    var mouseY = e.pageY - this.offsetTop;
    /* x and y positions*/
    pressed = true;
    addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
    redraw();
    /*add location and draw again*/
  });
  
  $('#canvas').mousemove(function(e){
    if(pressed){
      addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
      redraw();
      /* add locations while moving if pressed is true*/
    }
  });

  $('#canvas').mouseup(function(e){
    pressed = false;
  });
  $('#canvas').mouseleave(function(e){
    pressed = false;
  });
  // $('#canvas').css({"cursor": "url(img/green.cur)",  "-moz-zoom-in, auto"});

  function redraw(){
    canvas.width = canvas.width; // Clears the canvas
    
    context.lineJoin = "round";
    context.lineWidth = 5;

    for(var i=0; i < clickX.length; i++)
    {   
      context.beginPath();
      if(clickDrag[i] && i){
        context.moveTo(clickX[i-1], clickY[i-1]);
      }else{
       context.moveTo(clickX[i]-1, clickY[i]);
     }
     context.lineTo(clickX[i], clickY[i]);
     context.closePath();
     context.strokeStyle = clickColor[i];
     context.stroke();
   }

 }
   $('.eraser').click(function(){
      if(curTool == "eraser"){
        curTool = "pen";
        $('.eraser').html("Change to eraser");
      }
      else{
        curTool = "eraser";
        $('.eraser').html("Change to pen");
      }
   })
   $('.save').click(function() {
    var saved_image = document.getElementById('canvas').toDataURL("image/png");            
    $.ajax({
      type: "POST",
      url: "script.php",
      data: { 
      imgBase64: saved_image
    }
      }).done(function(o) {
      console.log('saved');
    });
   })

   // $('.restore').click(function() {
   //  // context.restore();
   //  // redraw();
   //  document.write('<img src="'+saved_image+'"/>');
   // })
   $('.clear').click(function(){
    canvas.width = canvas.width;
    clickX = new Array(); /*location of X and Y*/
    clickY = new Array();
    clickDrag = new Array(); 
    clickColor = new Array();
    clickTool = new Array();
   })
 </script>

<script>
  var num = <?php echo $numOfImages ?>;
  var currentItem = 0,
  content = <?php echo '{extURL: [' . $hashes . ']}' ?>;

  $(function () {
    var s = null,
    // primary namespace begins here
    MyContent = {
      // below are the variables being used in the slider
      settings : {
        sliderWidth: $("#photo").width(),
        speed: 200, // higher = slower for moving speed
        // below is the time between each slide for the auto-sliding mechanism
        intervalPause: 2000, // works speed
        // the variables below are just caching stuff that's reused
        photoList: $("#photo ul"),
        canvas: $("#photo"),
        links: $("#navigation ul a"),
        hlLinks: "#navigation a.highlight",
        hlClass: "highlight",
        prevNext: $(".prev-next"),
        pnItem: 0,
        // below are just initial values for later-used stuff
        marginSetting: null,
        currentView: null,
        activeItem: null
      },

      // function to populate the data for the clicked item; data comes from file: "js/data.js"
      init: function (currentItem, content) {
        s = this.settings;
        $("#title>a")[0].href = content.extURL[currentItem];
      },
      prepPreviousNext: function () {

              // $(window).keyup(function(event){
              //   if(event.which == 37){
                  
              //   }

              // })
        s.prevNext.bind("click", function () {
          s.activeItem = $("#navigation a.highlight")[0].href.split("#")[1];

          // make sure the value from the URL is a number, otherwise addition operator won't work
          s.activeItem = parseInt(s.activeItem, 10);

          if ($(this).attr("id") === "prev") {
            s.pnItem = (s.activeItem + num - 1) % num;
          } else {
            s.pnItem = (s.activeItem + num + 1) % num;
          }

          $(s.links).removeClass(s.hlClass);
          MyContent.doPreviousNext(s.activeItem, s.pnItem);
          return false;
        });
      },
      
      doPreviousNext: function (activeItem, pnItem) {
        s = this.settings;
        $("#navigation ul li:nth-child(" + (s.pnItem + 1) + ") a").addClass(s.hlClass); //NTH-CHILD
        if (s.pnItem > 0) {
          s.marginSetting = s.sliderWidth * s.pnItem;
          s.photoList.animate({
            marginLeft: "-" + s.marginSetting + "px"
          }, s.speed);
      
        } else {
          s.photoList.animate({
            marginLeft: "0px"
          }, s.speed);
        }
        this.init(s.pnItem, content);
      },
    };  
    MyContent.init(currentItem, content);
    MyContent.prepPreviousNext();
  });
</script>
<script>
  // window.setInterval(function(){

  // })
</script>



       </body>
       </html>