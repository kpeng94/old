
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>realMITe</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
<!--    <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">-->
    <!-- <link href="../bootstrap/css/bootstrap.css" rel="stylesheet"> -->
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <!-- <link href="../bootstrap/css/bootstrap-responsive.css" rel = "stylesheet"> -->
    <link href="css/custom.css" rel="stylesheet">
  </head>
    <script src="http://code.jquery.com/jquery-1.9.0.min.js"></script>

  <body>
  <?php
  session_start();
  require("db.php");
  $user = $_SESSION['username']; 
  $type = $_SESSION['type'];
  if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
    $first = $_SESSION['first'];
    $last = $_SESSION['last'];
  }
  else{
    header("location:index.php");
  }
  // Get informatio
  $class_query = "SELECT CLASS from classes WHERE USERNAME='" . mysql_real_escape_string($user) . "' ORDER BY CLASS";
  $class_result = mysql_query($class_query, $db) or die(mysql_error());
  $choices = "";
  $classes = "";
  $class_select ="";  
  $select_query = "SELECT CLASS from catalog ORDER BY CLASS";
  $select_result = mysql_query($select_query, $db) or die(mysql_error());
  while($row = mysql_fetch_assoc($select_result)){
    $choices = $choices. '<option value ="' . $row["CLASS"] . '">' . $row["CLASS"] . '</option>';
  }

  $lectures_list = "";
  
    if(mysql_num_rows($class_result) == 0 && $type == "student")
      $classes = "You are not enrolled in any classes!"; 
    else if (mysql_num_rows($class_result) == 0 && $type == "prof")
      $classes = "You are not instructing any classes!";
    else if (mysql_num_rows($class_result) == 0 && $type == "admin")
      $classes = "You are not administrating any classes!";
    else{
      $classes = "";      
      while($row = mysql_fetch_assoc($class_result)){
        $lecs = glob("upload/" . $row["CLASS"] . "/" . "*/" . "*.jpg");
        $lectureNum = 0;
        foreach($lecs as $lec){
          $dummy = explode("/",$lec);
          $lectureNum = $dummy[2];
        }
        $lectures_list = $lectures_list . '<img src= "img/folder.png" height = 12 width = 12"/> ' . $row["CLASS"] . '/<br />';
        for($i = 1; $i < $lectureNum; $i++)
          $lectures_list = $lectures_list . '<a href = "http://kpeng94.scripts.mit.edu/myProjects/6.470/realtime/classrooms/' . $row["CLASS"] . '/'. $i . '/redir.php">&nbsp;&nbsp;&nbsp;Lecture ' . $i . '</a><br />';
        if($lectureNum != 0)
          $classes = $classes . '<a href = "http://kpeng94.scripts.mit.edu/myProjects/6.470/realtime/classrooms/' . $row["CLASS"] . '/'. $lectureNum . '/redir.php">' . $row["CLASS"] . "</a> &nbsp; ";
        if($lectureNum == 0)
          $classes = $classes . $row["CLASS"] . ' &nbsp; ';
        $class_select = $class_select . '<option value = "' . $row["CLASS"] . '">' . $row["CLASS"] . '</option>';
      }
    }
  ?>
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          </a>
          <a class="brand" href="#">realMITe</a>
          <div class="nav-collapse collapse">
            <ul class="nav pull-right">
              <li class="dropdown">
                <a href="#" class ="dropdown-toggle" data-toggle="dropdown">Welcome, <?php echo("$first" . " " . "$last");  ?>!<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="index2.php">My Profile</a></li>
                  <li class="divider"></li>
                  <li><a href="#" id = "logout">Logout</a></li>
                </ul>                
              </li>
            </ul>

          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
    <div class="container">
  <?php
    $partner_query = "SELECT * FROM groups WHERE REQUEST = 'no' and CLASS = '$classNumber'";
    $partner_result = mysql_query($partner_query, $db) or die(mysql_error());
    while($partner_row = mysql_fetch_assoc($partner_result)){
      if($user == $partner_row["USER1"])
        $partner = $partner_row["USER2"];
      else if($user == $partner_row["USER2"])
        $partner = $partner_row["USER1"];
    }

  ?>

  <?php
  if ($type == "student"){
  ?>
      <div class="row">
        <div class="textspan3" style="height:738px">
          <h2>Classes I am enrolled in:</h2>
          <h4>(Click to proceed to classroom)</h4>
  <?php echo $classes ?>
        </div>
        <div class = "textspan3 scroll" style="height:738px">
          <h2>My Lectures</h2>
          <?php echo $lectures_list;?>
        </div>
        <div class = "textspan3" style="height:738px">
          <h2>Enroll in a class:</h2>
          <form action="enroll.php" method="post">
          <select name = "enroll_select"> 
            <?php echo $choices ?>
          </select>
          <button class = "btn btn-mini" type="submit" name="submit" value="Submit">Submit </button>
          </form>
          <?php
            if(isset($_SESSION["approve"])){
              echo $_SESSION["approve"];
              unset($_SESSION["approve"]);
            }
          ?>          
          <br /><br />
          <h2>Pending Approval:</h2>
          <?php
          $approve_pending = "";
          $approve_query = "SELECT CLASS FROM approve WHERE USERNAME = '$user'";
          $approve_result = mysql_query($approve_query, $db) or die(mysql_error());
          while($approve_row = mysql_fetch_assoc($approve_result)){
            $approve_pending = $approve_pending . $approve_row["CLASS"] . "<br />";
          }
          echo $approve_pending;
          ?>
        </div>
        <div class="textspan3" style="height:738px">
          <h2>Partners:</h2>
            <?php
              $need_approval = "";
              $class_option = "";
              $class_result = mysql_query($class_query, $db) or die(mysql_error());
              while($row = mysql_fetch_assoc($class_result)){
                $class_name = $row["CLASS"];
                $class_option = $class_option . '<option value ="' . $row["CLASS"] . '">' . $row["CLASS"] . '</option>';
                $need_approval = $need_approval . "<h4>" . $class_name . "</h4>";
                $query = "SELECT USER2 FROM groups WHERE CLASS = '$class_name' and USER1 = '$user' and REQUEST = 'no'";
                $result = mysql_query($query, $db) or die(mysql_error());
                while($row2 = mysql_fetch_assoc($result)){
                  $need_approval = $need_approval . $row2["USER2"] . "<br />";
                }
              }
              echo $need_approval;
            ?>
          <br />
          <h2>Request Team Members:</h2>    
          <form action="requestPartner.php" method="post">
          <input type = "text" name = "request_p"></input>
          <select name = "class_req">
            <?php echo $class_option ?>            
          </select><br>
          <button class = "btn" type="submit" name="submit" value="Submit">Submit </button>
          </form>
          <h2>Pending Requests:</h2>
            <h4>From Others:</h4>
            <?php
              $req_a = ""; 
              $req_p = "";
              $query_req = "SELECT * FROM groups WHERE USER2 = '$user' and REQUEST='yes'";
              $result_req = mysql_query($query_req) or die(mysql_error());
              while($row = mysql_fetch_assoc($result_req)){
                $req_a = $req_a . $row["CLASS"] . ": ". $row["USER1"] . '<form action="acceptPartner.php" method="post"><input type = "hidden" name = "classb" value = "' . $row["CLASS"] . '"><input type = "hidden" name = "u1" value = "' . $row["USER1"] . '"><input type = "hidden" name = "u2" value = "'. $row["USER2"] . '"><input type = "radio" value ="accept" name = "classa">Accept <input type = "radio" value ="decline" name = "classa">Decline <button class = "btn btn-mini" type="submit" name="submit" value="Submit">Submit </button></form>';
              }
              echo $req_a;
            ?>
            <h4>Your Requests:</h4>
            <?php
              $query_reqp = "SELECT * FROM groups WHERE USER1 = '$user' and REQUEST='yes'";
              $result_reqp = mysql_query($query_reqp) or die(mysql_error());
              while($row = mysql_fetch_assoc($result_reqp)){
                $req_p = $req_p . $row["CLASS"] . ": ". $row["USER2"] . "<br />";
              }
              echo $req_p;
            ?>

          <?php
          ?>      
        </div>
  <?php }
  if ($type == "prof" || $type == "admin"){
  ?>
      <div class="row">
        <div class="textspan4" style="height:738px">
          <h2>Classes I am teaching:</h2>
  <?php 
        echo $classes;
        if (mysql_num_rows($class_result) != 0){
  ?>
        <br />
        <h2> Upload Files: </h2>
        <h4> Upload your image files in a .jpg format and name them in the order that you want them to appear (Max 20 at once). </h4>
          <form action="upload_file.php" method="post" enctype="multipart/form-data">
          <input type="file" multiple name="file[]" id="file" value =""><br />
          Lecture Number: <input type="text" name = "lecture" value = "0"><br />
          <!-- Choose class to input for here -->
          <select name = "upload_select"> 
            <?php echo $class_select ?>
          </select>
          <button class = "btn-small" type="submit" name="submit" value="Submit">Submit </button>
          </form>
          <?php
            if(isset($_SESSION["uploads"])){
              echo $_SESSION["uploads"];
              unset($_SESSION["uploads"]);
            }
          } ?>
        </div>
        <div class = "textspan4 scroll" style="height:738px">
          <h2>My Files</h2>
          <?php
          $directory = "upload/";
          $last1 = "";
          $last2 = ""; 
          $file_list = "<ul>";
          $class_result = mysql_query($class_query, $db) or die(mysql_error());
          while($row = mysql_fetch_assoc($class_result)){          
            $images = glob($directory . "" . $row["CLASS"] . "/" . "*/" . "*.jpg");
            foreach($images as $image)
            {
              $explode_images = explode("/",$image);
              if($explode_images[2]!=$last2 || $explode_images[1]!=$last1)
                $file_list = $file_list . '</ul><ul class = "files"><img src= "img/folder.png" height = 12 width = 12> ' . $explode_images[1] . "/" . $explode_images[2] . "/" . "<br />";
              $file_list = $file_list . '<li>'. $explode_images[3] . "</li>";
              $last1 = $explode_images[1];
              $last2 = $explode_images[2];
            }
            $file_list = $file_list . '</ul>';
          }
          echo $file_list;
          ?>
        </div>
        <div class="textspan4" style="height:738px">
          <h2>These Users Need Approval for Your Classes:</h2>
            <?php
              $need_approval = "";
              $class_result = mysql_query($class_query, $db) or die(mysql_error());
              while($row = mysql_fetch_assoc($class_result)){
                $class_name = $row["CLASS"];
                $need_approval = $need_approval . "<h4>" . $class_name . "</h4>";
                $query = "SELECT USERNAME FROM approve WHERE CLASS = '$class_name'";
                $result = mysql_query($query, $db) or die(mysql_error());
                while($row2 = mysql_fetch_assoc($result)){
                  $need_approval = $need_approval . $row2["USERNAME"] . "<br />";
                }
              }
              echo $need_approval;
            ?>
        </div>

          <?php
} ?>

	  <br />
    <br />

      </div>

    </div> <!-- /container -->

  <script>
    $(document).ready(function(){
      $("#logout").click(function(){
        window.location = "logout.php";
      });
    });    
  </script>
    <script src="css/bootstrap-dropdown.js"></script>
  </body>
</html>
